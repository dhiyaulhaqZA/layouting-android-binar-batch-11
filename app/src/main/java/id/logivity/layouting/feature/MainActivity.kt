package id.logivity.layouting.feature

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import id.logivity.layouting.R
import id.logivity.layouting.preferences.AppDataPreferences
import kotlinx.android.synthetic.main.activity_edit_profile.*
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    private lateinit var appData: AppDataPreferences

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        setupComponent()
        setupListener()
    }

    override fun onResume() {
        super.onResume()
        tv_main_username.text = appData.getUsername()
    }

    private fun setupComponent() {
        appData = AppDataPreferences(this)
    }

    private fun setupListener() {
        btn_main_edit_profile.setOnClickListener {
            startActivity(Intent(this@MainActivity, EditProfileActivity::class.java))
        }

        btn_main_logout.setOnClickListener {
            appData.cleanUserData()
            startActivity(Intent(this@MainActivity, LoginActivity::class.java))
            finish()
        }
    }
}
