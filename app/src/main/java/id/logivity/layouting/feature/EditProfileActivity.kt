package id.logivity.layouting.feature

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import id.logivity.layouting.R
import id.logivity.layouting.preferences.AppDataPreferences
import kotlinx.android.synthetic.main.activity_edit_profile.*

class EditProfileActivity : AppCompatActivity() {

    private lateinit var appData: AppDataPreferences

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_edit_profile)

        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        setupComponent()
        setupListener()
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        return when (item?.itemId) {
            android.R.id.home -> {
                finish()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    private fun setupComponent() {
        appData = AppDataPreferences(this)
        et_username.setText(appData.getUsername())
    }

    private fun setupListener() {
        btn_username_save.setOnClickListener {
            val username = et_username.text.toString().trim()
            appData.putUsername(username)
            finish()
        }
    }
}
