package id.logivity.layouting.feature

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import id.logivity.layouting.preferences.AppDataPreferences

class SplashScreenActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val appData = AppDataPreferences(this)
        val isUserLoggedIn = appData.getIsUserLoggedIn()
        if (isUserLoggedIn) {
            routeTo(MainActivity::class.java)
        } else {
            routeTo(LoginActivity::class.java)
        }
    }

    private fun routeTo(cls: Class<*>) {
        val intent = Intent(this@SplashScreenActivity, cls)
        startActivity(intent)
        finish()
    }
}
